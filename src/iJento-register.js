mmcore.IntegrationFactory.register('iJento', {
  defaults: {},

  validate: function (data) {  
    if(!data.campaign)
      return 'No campaign.';
    return true;  
  },

  check: function (data) {  
    return window.si && window.si.sendAdditionalTracer;
  },

  exec: function (data) {  
    var prodSand = data.isProduction ? 'MM_Prod' : 'MM_QA',
        mvtData = prodSand + "_" + data.campaignInfo;

    window.si.sendAdditionalTracer("/MVT", "creativeID=" + mvtData);
    
    if (data.callback) data.callback();
    return true;
  }
});