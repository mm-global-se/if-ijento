# iJento

---

[TOC]

## Overview

This integration is used for extracting campaign experience data from the Maxymiser platform and sending it to __iJento__.

## How we send the data

We are calling `sendAdditionalTracer` method of the `window.si` object, provided by __iJento__, passing campaign generation data as an argument (see data format [below](#data-format))

_Example_:

```javascript
window.si.sendAdditionalTracer("/MVT", "creativeID=MM_Prod_T1Button=colr:red");
```

## Data Format

The data sent to iJento will be in the following format:

`<mode>_<campaign name>=<element1>:<variant>|<element2>:<variant>`

Please find additional information on the format of the sent data [here](https://bitbucket.org/mm-global-se/integration-factory#markdown-header-introduction
)

## Download

* [iJento_register.js](https://bitbucket.org/mm-global-se/if-ijento/src/master/src/iJento-register.js)

* [iJento_initialize.js](https://bitbucket.org/mm-global-se/if-ijento/src/master/src/iJento-initialize.js)

## Prerequisite

The following information needs to be provided by the client: 

+ Campaign Name


## Deployment instructions

### Content Campaign

+ Ensure that you have the Integration Factory plugin deployed on site level([find out more](https://bitbucket.org/mm-global-se/integration-factory)). Map this script to the whole site with an _output order: -10_ 

+ Create another site script and add the [iJento_register.js](https://bitbucket.org/mm-global-se/if-ijento/src/master/src/iJento-register.js) script.  Map this script to the whole site with an _output order: -5_

    __NOTE!__ Please check whether the integration already exists on site level. Do not duplicate the Integration Factory plugin and the Google Analytics Register scripts!

+ Create a campaign script and add the [iJento_initialize.js](https://bitbucket.org/mm-global-se/if-ijento/src/master/src/iJento-initialize.js) script. Customise the code by changing the campaign name, account ID (only if needed), and slot number accordingly and add to campaign. Map this script to the page where the variants are generated on, with an _output order: 0_

### Redirect Campaign

For redirect campaigns, the campaign script needs to be mapped to both the generation and redirected page. To achieve this follow the steps bellow:

+ Go through the instructions outlined in [Content Campaign](https://bitbucket.org/mm-global-se/if-ga#markdown-header-content-campaign)

+ Make sure that in the initialize campaign script, redirect is set to true.

+ Create and map an additional page where the redirect variant would land on. 

+ Map the integration initialize campaign script to this page as well.

## QA

### sFiddler

Save [SiTrackerInspector.dll](https://bitbucket.org/mm-global-se/if-ijento/src/e2ec5da1be3398162993e6029184099639171fa9/qa/SiTrackerInspector.dll?at=master&fileviewer=file-view-default) file to the program files of fiddler/inspectors folder and make sure it is called 'SiTrackerInspector.dll'. 
After the data is successfully sent to iJento in Fiddler look for a url that starts with '/si/track.gif...' (Note that there will be multiple calls made to this URL so you might have to look through all of them to find the relevant one). Click on inspector/Site Tracker. Our experience data should be under Query.